#!/usr/bin/env bash

# purpose: create Shepards Bash Pie script
# created by: Ilay ron
# date: 18/11/2020
# version: v1.1.5
###################################################


main(){

Heat_it_up

Ingridient_for_shepard_pie

create_extended_partition

create_mount_disk_fridge

sort_sheperad_pie_array



}

Heat_it_up(){


for cel in {1..180}
do

if [[ $cel -le 180 ]]
then	
#wait 20 second before the next number to wait every 3 minute for the cron job

	let cel++;
	sleep 20s

	crontab -e */3 * * * * $cel << EOL
	echo "the current celsius is: $cel"
#if the celsus is 180 i want to stop and let the user to know that the oven is ready
elif [[ $cel -eq 180 ]]
then
 break
echo "The oven on $cel and ready for the pie"

fi
done

}

Ingridient_for_shepard_pie(){
#create the array of the ingridients for the sheperd pie
#ingridients=("minced_meat","mash_potatoes","salt","peper","quit")

echo "please select the ingridient for th shepard pie: "

ingridients=("minced_meat" "mash_potatoes" "salt" "peper")

# create ingridient for the sheperad pie

for ing in "${ingredients[$@]}"
do

if [[ -z "${ingredients[$1]}" ]]
then

echo "The $ingridients[$1] added successfully"
elif [[-z "${ingridients[$2]}" ]]
then

echo "The $ingridients[$2] added successfully"
elif [[ -z "${ingridients[$3]}" ]]
then

echo "The $ingridients[$3] added successfully"
elif [[ -z "${ingridients[$4]}" ]]
then

echo "The $ingridients[$4] added successfully"

else

echo "invalid ingredient please select the correct ingridient for the sheperad pie"
fi
done
	
}

create_extended_partition(){


fdisk /dev/sdd1  << EOF
n
e


+500M
w
EOF
fi

}

create_mount_disk_fridge(){

mount /dev/sdd1 /mnt/fridge

}

sort_sheperad_pie_array(){
Shapered_bash_array=("minced_meat" "mashed_potetos" "salt" "peper")
IFS=$'\n' sorted=($(sort <<<"${sheperad_bash_array[*]}")); unset IFS
printf "[%s]\n" "${sorted[$@]}"

$sorted[$@] > fridge/ingridients.array

}


main
